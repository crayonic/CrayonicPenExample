package com.crayoni.pen.sample1;

import android.Manifest;
import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;


import com.crayonic.sigma.crypto.craytoken.BasicComm;
import com.crayonic.sigma.crypto.craytoken.LineResponseListener;
import com.crayonic.signatureview.SignatureController;
import com.crayonic.signatureview.SignatureView;
import com.crayonic.signatureview.model.Point;
import com.crayonic.signatureview.model.Signature;
import com.crayonic.pen.sample1.BuildConfig;
import com.crayonic.pen.sample1.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import timber.log.Timber;

public class SigningActivity extends AppCompatActivity implements LineResponseListener {

    private static final int MODE_NO_PEN = 0;
    private static final int MODE_SIGN = 1;
    private static final int MODE_SHOW = 2;
    private static final int MODE_DATA = 3;
    private static final int MODE_TRAIN = 4;
    private static final String TAG = SigningActivity.class.getSimpleName();
    private static final int WRITE_PERM_CODE = 111;
    private static final int LOCAT_PERM_CODE = 1;

    private TextView mTextMessage;
    private SignatureView mSignatureView;
    private ScrollView mScrollView;
    private TextView mDataText;
    private TextView mDataSubtitle;
    private Context context;

    private SignatureController mSignatureController;
    private BasicComm mPenComm;


    private int mMode = MODE_SIGN;
    private View mCoordinator;
    private ImageView mSignatureImage;
    private View loading_cycle;
    private View loading_icon;
    private ObjectAnimator loadingAnimator;
    private FloatingActionButton controlButtonReload;
    private FloatingActionButton controlButtonSwitch;
    private boolean isToggleData = true;
    private View mSignatureImageFrame;
    private View loading_scan;


    private View loading_frame;
    private FloatingActionButton controlButtonShare;
    private Bitmap bitmapForShare = null;

    /**
     * Change the modus of operand. NO_PEN -> SIGN -> SHOW/DATA -> NO_PEN
     *
     * @param mode
     * @return
     */
    private boolean changeMode(int mode) {
        Log.d(TAG, "changeMode() called with: mode = [" + mode + "]");
        mMode = mode;
        switch (mode) {
            case MODE_NO_PEN:
                //mPenComm.disconnectPen();
                mPenComm.startScanForPen();

                mTextMessage.setVisibility(View.VISIBLE);
                mScrollView.setVisibility(View.GONE);
                mSignatureView.setVisibility(View.VISIBLE);
                mSignatureImage.setVisibility(View.GONE);
                mSignatureImageFrame.setVisibility(View.GONE);

                loading_frame.setVisibility(View.GONE);
                controlButtonReload.setVisibility(View.GONE);
                controlButtonSwitch.setVisibility(View.GONE);
                controlButtonShare.setVisibility(View.GONE);


                try {
                    mSignatureView.clearCanvas();
                } catch (IllegalArgumentException firstStartException) {
                    Log.i(TAG, "changeMode: starting for first time from onCreate");
                }

                writeLine("Scanning for Crayonic devices");
                textInToast("Looking for pen...");

                mSignatureController.switchMode(SignatureController.MODE_SHOW); // ignore touch
                mSignatureView.setMode(SignatureView.SIGNING);



                return true;

            case MODE_SIGN:
                if (!mPenComm.isConnected()) {
                    //sanity check, just in case it hung up
                    changeMode(MODE_NO_PEN);
                }

                mTextMessage.setVisibility(View.VISIBLE);
                mScrollView.setVisibility(View.GONE);
                mSignatureView.setVisibility(View.VISIBLE);
                mSignatureImage.setVisibility(View.GONE);
                mSignatureImageFrame.setVisibility(View.GONE);

                loading_frame.setVisibility(View.GONE);
                controlButtonSwitch.setVisibility(View.GONE);
                controlButtonShare.setVisibility(View.GONE);

                try {
                    mSignatureView.clearCanvas();
                } catch (IllegalArgumentException firstStartException) {
                    Log.i(TAG, "changeMode: starting for first time from onCreate");
                }

                writeLine("Sign here");
                textInToast("Signing ready");

                mSignatureController.switchMode(SignatureController.MODE_SIGNING);
                mSignatureView.setMode(SignatureView.SIGNING);


                return true;



            case MODE_DATA:
                loading_frame.setVisibility(View.GONE);
                mTextMessage.setText(R.string.title_notifications);

                mTextMessage.setVisibility(View.GONE);
                mScrollView.setVisibility(View.VISIBLE);
                mSignatureView.setVisibility(View.GONE);
                mSignatureImage.setVisibility(View.GONE);
                mSignatureImageFrame.setVisibility(View.GONE);

                controlButtonSwitch.setVisibility(View.VISIBLE);
                controlButtonShare.setVisibility(View.VISIBLE);
                controlButtonReload.setVisibility(View.VISIBLE);

                displaySignatureData(mSignatureController.getSignatureData());
                isToggleData = true;
                controlButtonSwitch.setImageResource(R.drawable.ic_pencil);
                return true;

            case MODE_SHOW:
                loading_frame.setVisibility(View.GONE);
                mSignatureController.switchMode(SignatureController.MODE_SHOW);

                mSignatureImage.setImageBitmap(mSignatureController.getSignatureBitmap());

                mTextMessage.setVisibility(View.GONE);
                mScrollView.setVisibility(View.GONE);
                mSignatureView.setVisibility(View.GONE);
                mSignatureImage.setVisibility(View.VISIBLE);
                mSignatureImageFrame.setVisibility(View.VISIBLE);

                controlButtonSwitch.setVisibility(View.VISIBLE);
                controlButtonShare.setVisibility(View.VISIBLE);
                controlButtonReload.setVisibility(View.VISIBLE);

                isToggleData = false;
                controlButtonSwitch.setImageResource(R.drawable.ic_table_scroll);

                return true;


        }

        return false;


    }

    /**
     * Animate loading - rotation of view
     * No timeout, animation should be stopped when necessary by loadingStop(..)
     *
     * @param view to be VISIBLE and rotated, e.g. arrows or circular gradient
     * @return Android ObjectAnimator reference that has already started
     */
    public static ObjectAnimator loadingStart(View view) {
        view.setVisibility(View.VISIBLE);
        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(
                view,
                "rotation", 0f, 360f);
        objectAnimator.setDuration(1500);
        objectAnimator.setRepeatMode(ValueAnimator.RESTART);
        objectAnimator.setRepeatCount(Animation.INFINITE);
        objectAnimator.start();
        return objectAnimator;
    }

    /**
     * Stop animation and hide the view that was animated
     *
     * @param objectAnimator to be stopped
     * @param view           to be GONE
     */
    public static void loadingStop(ObjectAnimator objectAnimator, View view) {
        try {
            objectAnimator.cancel();
            view.setVisibility(View.GONE);
        } catch (NullPointerException someNull) {
            Log.i(TAG, "loadingStop: one of params is probably null : objectAnimator:" + objectAnimator + " view:" + view);
        }
    }

    private void btLoading(final boolean isLoading) {
        SigningActivity.this.runOnUiThread(new Runnable() {
            public void run() {
//        loadingStop(loadingAnimator, loading_cycle);
                if (isLoading) {
                    loading_frame.setVisibility(View.VISIBLE);
                    loading_icon.setVisibility(View.VISIBLE);
                    loadingAnimator = loadingStart(loading_cycle);
                } else {
                    loading_frame.setVisibility(View.GONE);
                    loading_icon.setVisibility(View.GONE);
                    loadingStop(loadingAnimator, loading_cycle);
                }
            }
        });

    }

    private void genericLoading(final boolean isLoading) {
        SigningActivity.this.runOnUiThread(new Runnable() {
            public void run() {
//        loadingStop(loadingAnimator, loading_cycle);
                if (isLoading) {
                    loading_frame.setVisibility(View.VISIBLE);
                    loadingAnimator = loadingStart(loading_cycle);
                } else {
                    loading_frame.setVisibility(View.GONE);
                    loadingStop(loadingAnimator, loading_cycle);
                }
            }
        });

    }

    /*
    * Animation effect and little bit of delay
    * contains callback that continues the signature creation pipeline.
    * */
    private void scanLoading() {
//            loadingStop(loadingAnimator, loading_scan);

        SigningActivity.this.runOnUiThread(new Runnable() {
            public void run() {

                loading_scan.setVisibility(View.VISIBLE);
                ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(
                        loading_scan,
                        "translationX", 0, mSignatureView.getWidth());
                Log.d(TAG, "loadingScanner: masterView width : " + mSignatureView.getWidth());
                objectAnimator.setDuration(500);
                objectAnimator.setRepeatMode(ValueAnimator.REVERSE);
                objectAnimator.setRepeatCount(3);
                objectAnimator.addListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        changeMode(MODE_SHOW);
                        loading_scan.setVisibility(View.GONE);

                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                });
                objectAnimator.start();

            }
        });

    }

    /**
     * Set texts to Signature Data table views
     *
     * @param synchronizedSignatureData
     */
    private void displaySignatureData(ArrayList<Point> synchronizedSignatureData) {
        mDataSubtitle.setVisibility(View.VISIBLE);
        mDataText.setVisibility(View.VISIBLE);

        mDataSubtitle.setText(new StringBuilder().append("Number of data points: ").append(synchronizedSignatureData.size()));

        mDataText.setText(signatureDataBuilder(synchronizedSignatureData, false));
    }

    /**
     * Create text from signature data
     *
     * @param signatureData
     * @param allData
     * @return
     */
    private String signatureDataBuilder(ArrayList<Point> signatureData, boolean allData) {
        StringBuilder sb = new StringBuilder(100000);
        if (allData) {
            //header
            sb.append("Digital Signature by Crayonic PenSDK").append("\n-\n");
            sb.append("Number of data points: ").append(signatureData.size()).append("\n-\n");
        }

        for (Point p : signatureData) {
            sb.append("Time: ").append(String.format("%04d", (int) p.time))
                    .append(" X: ").append(String.format("%04d", (int) p.x))
                    .append("  Y: ").append(String.format("%04d", (int) p.y))
                    .append(" Pressure: ").append(String.format("%02.4f", p.pressure))
                    .append("\n");
        }
        if (allData) {
            //footer
            long msTime = System.currentTimeMillis();
            Date curDateTime = new Date(msTime);
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            sb.append("\n-\n").append("Created by Crayonic PenSDK\nat ").append(formatter.format(curDateTime));
        }
        return sb.toString();
    }

    /**
     * Set layout view fields and properties of the activity view.
     * Most of the callbacks are set in here.
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;


        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        } else {
            // Timber.plant(new CrashReportingTree());
        }

        //
        setContentView(R.layout.sign_sample);

//        .setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
//                | View.SYSTEM_UI_FLAG_FULLSCREEN
//                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
//                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
//                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
//                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

        // animation related fields
        loading_cycle = findViewById(R.id.loading_icon_loading);
        loading_icon = findViewById(R.id.loading_icon_info);
        loading_scan = findViewById(R.id.loading_scanner_line);
        loading_frame = findViewById(R.id.loading_frame);
        mCoordinator = findViewById(R.id.coordinator_view);

        // MODE_SHOW fields : mSignatureImage, mSignatureImageFrame
        mSignatureImage = (ImageView) findViewById(R.id.signature_image);
        mSignatureImage.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                // copy data to clipboard
                genericLoading(true);
                if (bitmapForShare != null && !mSignatureController.notFinalized()) {
                    Log.d(TAG, "onLongClick: Copy Image");
                    Uri bmpUri = prepareImageUri("Signature Image");
                    final Intent shareIntent = new Intent(Intent.ACTION_SEND);
                    shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    shareIntent.putExtra(Intent.EXTRA_STREAM, bmpUri);
                    shareIntent.setType("image/png");
                    try {
                        context.startActivity(shareIntent);
                    } catch (Exception safetyNet) {
                        Log.e(TAG, "onClick: mSignatureImage : No activity can handle such Share Request !!! Be more humble and constrain yourself.", safetyNet);
                    }
                }
                genericLoading(false);
                return false;
            }
        });
        // used for show/hide of the signature image in MODE_SHOW
        mSignatureImageFrame = findViewById(R.id.signature_image_frame);

        // MODE_DATA related fields
        mDataText = (TextView) findViewById(R.id.signature_data);
        mDataSubtitle = (TextView) findViewById(R.id.signature_data_count);
        mScrollView = (ScrollView) findViewById(R.id.scroll_view);
        mDataText.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                // copy data to clipboard
                genericLoading(true);
                if (!mSignatureController.notFinalized()) {
                    Log.d(TAG, "onLongClick: Copy Data");
                    ClipData myClip = ClipData.newPlainText(
                            "Signature Data",
                            signatureDataBuilder(mSignatureController.getSignatureData(), false));
                    ClipboardManager myClipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);

                    myClipboard.setPrimaryClip(myClip);
                    textInToast("Signature Data copied");
                }
                genericLoading(false);
                return false;
            }
        });


        // MODE_SIGN  fields
        mSignatureView = (SignatureView) findViewById(R.id.signature_view);
        mTextMessage = (TextView) findViewById(R.id.message);


        // request permission then instantiate  pen communication
        requestPermission(Manifest.permission.ACCESS_COARSE_LOCATION, LOCAT_PERM_CODE);
        mPenComm = new BasicComm(getApplicationContext());
        mPenComm.setLineResponseListener(this);

        // set pen communication to Signature View and Controller
        mSignatureView.setPenComm(mPenComm);

        // set response text view showing important messages from SignatureView
        mSignatureView.setStatusText(mTextMessage);

        mSignatureController = new SignatureController(
                // set pen communication to Signature  Controller
                mPenComm,
                // set SignatureView for the visual feedback
                // * onTouch events are intercepted and handled in Controller first
                mSignatureView,

                // set Frontend Message handler aka Pen LineResponse listener
                // * it will pass the message back here
                this,

                // anonymous SignatureController.SignatureResponseListener : main workflow events are happening here
                new SignatureController.SignatureResponseListener() {
                    @Override
                    public void signingDone(Signature signature) {
                        textInToast("Signature Finalized");
                        Log.d(TAG, "signingDone() called with: signature = [" + signature + "]");
                        // start animation and delay before
                        scanLoading();
                    }

                    @Override
                    public void signingDone(ArrayList<Point> signaturePoints) {
                    }

                    @Override
                    public void trainingDone(ArrayList<Signature> signatures) {
                    }

                    @Override
                    public void signingDone(Bitmap signatureImage) {
                        bitmapForShare = signatureImage;
                    }

                    @Override
                    public void verificationDone(boolean isSuccess, double successRate) {
                        Log.d(TAG, "verificationDone() called with: isSuccess = [" + isSuccess + "], successRate = [" + successRate + "]");
                    }

                    @Override
                    public void signingError(int ERROR_CODE, String message, Exception exception) {
                        Log.d(TAG, "signingError() called with: ERROR_CODE = [" + ERROR_CODE + "], message = [" + message + "], exception = [" + exception + "]");
                        textInToast("Please reload and try again");
                        SigningActivity.this.runOnUiThread(new Runnable() {
                            public void run() {
                                changeMode(MODE_NO_PEN);
                            }
                        });
                    }

                    @Override
                    public void penCommunicationStarted(String message) {
                        Log.d(TAG, "penCommunicationStarted() called with: message = [" + message + "]");
                        // we care about connection state only in MODE_NO_PEN
                        if (mMode == MODE_NO_PEN) {
                            textInToast(message, Snackbar.LENGTH_SHORT, null, null);
                            btLoading(false);
                            writeLine("Ready for signing");
                            changeMode(MODE_SIGN);
                        }
                    }

                    @Override
                    public void penCommunicationEnded(String message) {
                        Log.d(TAG, "penCommunicationEnded() called with: message = [" + message + "]");
                        // we care about connection state only in MODE_NO_PEN
                        if (mMode == MODE_NO_PEN) {
                            textInToast(message, Snackbar.LENGTH_SHORT, null, null);
                        }
                    }


                    @Override
                    public void penCommunicationConnecting(String message) {
                        Log.d(TAG, "penCommunicationConnecting() called with: message = [" + message + "]");
                        // not worth to show message as the connection is too fast done
                    }

                    @Override
                    public void penCommunicationNotFound(String message) {
                        Log.d(TAG, "penCommunicationNotFound() called with: message = [" + message + "]");
                        // we care about connection state only in MODE_NO_PEN
                        if (mMode == MODE_NO_PEN) {

                            textInToast(message, Snackbar.LENGTH_SHORT, null, null);

                            if (!mPenComm.isConnected()) {
                                Log.e(TAG, "penCommunicationNotFound: restart btStack connection");
                                changeMode(MODE_NO_PEN);
                            }
                            textInToast("Pen connection restarted", Snackbar.LENGTH_SHORT, null, null);
                        }
                    }

                });


        //*
        // control buttons :
        //*

        // Reload back to MODE_SIGN
        controlButtonReload = (FloatingActionButton) findViewById(R.id.button_restart);
        controlButtonReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mMode != MODE_NO_PEN && mMode != MODE_SIGN)
                    changeMode(MODE_NO_PEN);
            }
        });

        // Toggle between MODE_VIEW and MODE_DATA
        controlButtonSwitch = (FloatingActionButton) findViewById(R.id.button_switch);
        controlButtonSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleModeDataShow();
            }
        });

        // Share Signature if possible - both image and data
        controlButtonShare = (FloatingActionButton) findViewById(R.id.button_share);
        controlButtonShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (bitmapForShare != null && !mSignatureController.notFinalized()) {
                        String title = "Digital Signature by Crayonic PenSDK";
                        String body = signatureDataBuilder(mSignatureController.getSignatureData(), true);
                        Uri bmpUri = prepareImageUri(title);
                        final Intent shareIntent = new Intent(Intent.ACTION_SEND);
                        shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        shareIntent.putExtra(Intent.EXTRA_STREAM, bmpUri);
                        shareIntent.putExtra(Intent.EXTRA_TEXT, body);
                        shareIntent.setType("image/png");

                        try {
                            context.startActivity(shareIntent);
                        } catch (Exception safetyNet) {
                            Log.e(TAG, "onClick: controlButtonShare : No activity can handle such Share Request !!! Be more humble and constrain yourself.", safetyNet);

                        }


                    } else {
                        textInToast("Cannot share un-complete Signature");
                    }

                } catch (Exception safetyNet) {
                    Log.e(TAG, "onClick: controlButtonShare + some really nasty error tried to kill the app ", safetyNet);

                }
            }
        });

        /*
        * Set initial MODE_NO_PEN - we have to connect it first before signature can be obtained
         */
        changeMode(MODE_NO_PEN);

    }

    /**
     * Helper to create Android Uri for an image through temporary storage.
     * On fail due to Permission denial or NullException triggers requestsPermission( .. WRITE_EXTERNAL_STORAGE .. ).
     *
     * @param title
     * @return
     */
    private Uri prepareImageUri(String title) {
        try {
            String pathofBmp = MediaStore.Images.Media.insertImage(
                    getContentResolver(),
                    bitmapForShare,
                    title,
                    "Crayonic PenSDK Signature Image"
            );
            Uri bmpUri = Uri.parse(pathofBmp);
            return bmpUri;
        } catch (NullPointerException secFail) {
            secFail.printStackTrace();
            Log.e(TAG, "onClick: controlButtonShare : have you requested the write permission ? ", secFail);
            requestPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, WRITE_PERM_CODE);
        }
        return Uri.parse("NOP");
    }


    private void requestPermission(String permissionString, int customResponseCode) {
        ActivityCompat.requestPermissions(SigningActivity.this,
                new String[]{
                        permissionString
                },
                customResponseCode);
    }

    /**
     * Toggle switch button between MODE_SHOW and MODE_DATA
     */
    private void toggleModeDataShow() {
        if (isToggleData) {
            changeMode(MODE_SHOW);
        } else {
            changeMode(MODE_DATA);
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    /**
     * Simple onRequestPermissionsResult implementation by android doc in AppCompat
     *
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d(TAG, "onRequestPermissionsResult() GRANTED with: requestCode = [" + requestCode + "], permissions = [" + permissions + "], grantResults = [" + grantResults + "]");

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(SigningActivity.this, "Permission denied to use Bluetooth/Location", Toast.LENGTH_SHORT).show();
                }
                break;
            }
            case 1111: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d(TAG, "onRequestPermissionsResult() GRANTED with: requestCode = [" + requestCode + "], permissions = [" + permissions + "], grantResults = [" + grantResults + "]");
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(SigningActivity.this, "Permission denied to Share Signatures / Write image to temporary storage", Toast.LENGTH_SHORT).show();
                }
                break;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    /**
     * Callback implementation. Sets text message to message text view. Runs on UI thread.
     *
     * @param line
     */
    @Override
    public void writeLine(final String line) {
        Log.d("Resp", line);
        SigningActivity.this.runOnUiThread(new Runnable() {
            public void run() {

                mTextMessage.setText(line);
            }
        });
    }


    /**
     * Create snack message on the bottom of the screen, LENGTH_INDEFINITE timeout, no action onClick
     *
     * @param text
     */
    private void textInToast(final String text) {
        textInToast(text, Snackbar.LENGTH_INDEFINITE, null, null);
    }

    /**
     * Create snack message on the bottom of the screen
     *
     * @param text
     * @param length         of timeout : one of Snackbar timeouts
     * @param actionText     action title e.g."clear me"
     * @param actionListener onClick
     */
    private void textInToast(final String text, int length, String actionText, View.OnClickListener actionListener) {
        Snackbar.make(mCoordinator, text, length)
                .setAction(actionText, actionListener).show();
    }

}
