#Crayonic Pen SDK Example
Sample app explaining the Pen SDK usage and options.

##Requisites 
- Minimal Android SDK is 16 but recommended minimal is 21.  
- Target SDK was tested with 26
- Recommended Android Support Library is 27 
- Permissions needed by Pen:
    - BLUETOOTH
    - BLUETOOTH_ADMIN
    - BLUETOOTH_PRIVILEGED
    - ACCESS_COARSE_LOCATION
- Permissions needed for signature sharing:
    - WRITE_EXTERNAL_STORAGE
    
    
##Import


Using Android Studio, import our maven repository into your main application build.gradle script.

```gradle
allprojects {
    repositories {
        maven {
            name 'Crayonic'
            url 'https://gitlab.com/crayonic/maven/raw/master/android/stable/'
        }
        
    }
}

```
Then attach sdk to your library dependencies.

##### Gradle 3.+ module build script
```gradle
    implement 'com.crayonic:pensdk:1.0'
```
##### Previous versions of Gradle module build script
```gradle
    compile 'com.crayonic:pensdk:1.0'
```


##SDK Usage
For basic use see the SigningActivity.
####Pen Init
Init necessary objects and get Bluetooth permissions in Activity onCreate(..).
```java
// bind Views
mSignatureView = (SignatureView) findViewById(R.id.signature_view);
mTextMessage = (TextView) findViewById(R.id.message);
 
// request permission then instantiate  pen communication
requestPermission(Manifest.permission.ACCESS_COARSE_LOCATION, LOCAT_PERM_CODE);
 
// create Basic Pen communication controller
mPenComm = new BasicComm(getApplicationContext());
mPenComm.setLineResponseListener(this);
 
// set Pen communication to Signature View and Controller
mSignatureView.setPenComm(mPenComm);
 
// set response text view showing important messages from SignatureView
mSignatureView.setStatusText(mTextMessage);
 
// create signature controller 
mSignatureController = new SignatureController(
                // set Pen communication to Signature  Controller
                mPenComm,
                
                // set SignatureView for the visual feedback
                // * onTouch events are intercepted and handled in Controller first
                mSignatureView,
                
                // set Frontend Message handler alias Pen LineResponse listener
                // * it will pass the messages from Pen back here
                this,
                
                // set Signature Response Listener 
                // * main workflow events are happening here
                mSignatureResponseListener
                );
                
```

####Pen Events
Catch Events through interface
```java

    public interface SignatureResponseListener {
        // not used in this sample
        void trainingDone(ArrayList<Signature> signatures);
        
        // used for obtaining Return Values after Pen finished signing
        void signingDone(Signature signature);
        void signingDone(ArrayList<Point> signaturePoints);
        void signingDone(Bitmap signatureImage);
        
        // Pen has disconnected from any reason (manual, Bluetooth, other)
        // standard process flow should be reset to default state
        void signingError(int ERROR_CODE, String message, Exception exception);
        
        // not used in this sample
        void verificationDone(boolean isSuccess, double successRate);
        
        // Pen state notification events 
        void penCommunicationStarted(String message);
        void penCommunicationEnded(String message);
        void penCommunicationNotFound(String message);
        void penCommunicationConnecting(String message);
    }

```
####Important methods
#####BasicComm
Start scanning for Crayonic Pen
```
    mPenComm.startScanForPen();
```

Do not call disconnect, rather use Pen Events for receiving notice of communication end. Pen is configured to end the communication after timeout pass


####Return objects
Return objects can be obtained either through Pen Events interface SignatureResponseListener or SignatureController. 
##### Signature model
Contains both 
 - List of Points
 - Image of Signature
#####List of Points
One Point ( time, x, y, pressure ) each 10 ms of signing. Starting with first touch of display.
#####Image of Signature
Image of the signature drawing.


